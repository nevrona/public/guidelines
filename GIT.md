# Git Guidelines

## First commit

Initialize every repository with an empty commit: `!git init && git commit -m 'Init repo' --allow-empty`. Tip: create an alias such as `it`.

## Flow

Use [git flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow). Merge all branches with fast-forward.  
Name flow branches like:

* feature/
* bugfix/
* release/
* hotfix/
* support/

Yes, with a slash.

You could also use git aliases too (see the section about aliases).

Don't use version tag prefixes.

The main (production release) branch MUST be named `main` instead of `master`. The *next release* branch MUST be name `develop`.

### TL;DR Git Flow Repo Initialization

From [git flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) site.

```
$ git init project && cd project && git flow init && git commit -m 'Init repo' --allow-empty
Initialized empty Git repository in ~/project/.git/
No branches exist yet. Base branches must be created now.
Branch name for production releases: [master] main
Branch name for "next release" development: [develop]

How to name your supporting branch prefixes?
Feature branches? [feature/]
Release branches? [release/]
Hotfix branches? [hotfix/]
Support branches? [support/]
Version tag prefix? []

$ git branch
* develop
 main
```

### Resources

* [A successful Git branching model](https://nvie.com/posts/a-successful-git-branching-model/)

## Branching

The main (production release) branch MUST be named `main` instead of `master`. The *next release* branch MUST be name `develop`.

For branch names, use very descriptive short names or the issue number along with the issue tracker in use (currently Gitlab only), i.e.: GL-1 for issue number one of that project.

## Commiting

Good commit messages are necessary to understand a project's history. For a merge request to be accepted, its commit message has to be more or less correct (not necessarily perfect). Follow [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/) and [Commit messages guide](https://github.com/RomuloOliveira/commit-messages-guide/blob/master/README.md).

Additionally, it becomes incredibly useful to be able to quickly link a commit with its issue, so use a preface with the issue number along with the tracker in use (currently Gitlab only), i.e.: `[GL-1] Fix foo incorrectly typed in bar lib`. You can use [a hook](https://github.com/ceph/ceph-deploy/blob/master/CONTRIBUTING.rst#commits) that prefaces the branch name for you (see also Branching section).

## Rebasing

It is very common to be afraid of rebasing, and it makes sense: you can actually break a repository by rebasing improperly. However, *modern technology* (say, Gitlab and Git Flow) can prevent this to happen in a very important branch so the harm would be mostly contained to your own branch. And truth be told, once accustomed to rebasing it becomes very useful and almost no error-prone.

For a commit history to be legible, rebasing is fundamental. Read [git rebase in depth](https://git-rebase.io/).

### Resources

* [Merging vs. Rebasing](https://www.atlassian.com/git/tutorials/merging-vs-rebasing)

## Ignoring

Use a convenient gitignore file from [Github's collection](https://github.com/github/gitignore/).

## Gitlab

To follow *git flow*, protect both `main` and `develop` branches at *Settings/Repository/Protected Branches* with the following settings:

| Branch | Allowed to merge | Allowed to push |
| ------ | ---------------- | --------------- |
| main   | Maintainers      | No one          |
| develop | Developers + Maintainers | Maintainers/No one (\*) |
| release/\* | Maintainers | Developers + Maintainers |

\* Depending on the project, it might be useful to treat develop so that nobody can directly push, but maybe as a more general rule allow maintainers to push.

Also add the following regex to protect tags from being created at
*Settings/Repository/Protected Tags*:

| Wildcard | Allowed to create | Result |
| -------- | ----------------- | ------ |
| 0.\* | Developers + Maintainers | Can create beta version tags |
| \*-dev | Developers + Maintainers | Can create test versions |
| \* | Maintainers | No tag can be deleted or updated |

Note: Gitlab currently doesn't support regex for better protection.

Configure merge request at *Settings/General/Merge requests* as *Fast-forward merge* and require checks for *Pipeline must succeed* (unless the project has no pipelines) and *All discussions must be resolved*.

## Aliases

This section is *not* mandatory.  
If you need some aliases, here are some:

```
[alias]
    ci = commit
    cis = commit -S
    co = checkout
    it = !git init && git checkout -b main && git commit -m 'Init repo' --allow-empty
    commend = commit --amend --no-edit
    please = push --force-with-lease
    st = status
    br = branch
    hist = log --graph --abbrev-commit --decorate --all --format=format:\"%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(dim white) - %an%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n %C(white)%s%C(reset) - %C(green)%G?%C(reset)\"
    histsig = log --graph --abbrev-commit --decorate --all --format=format:\"%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(dim white) - %an%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n %C(white)%s%C(reset) - %C(green)%G?%C(reset)% %C(white) sig from %GS (0x%GK)%C(reset)\"
    lg = log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(cyan)<%an>%Creset' --abbrev-commit --date=relative
[core]
    editor = vim
[gitflow "prefix"]
    feature = feature/
    bugfix = bugfix/
    release = release/
    support = support/
    hotfix = hotfix/
```

## Resources

* [git.WTF](https://git.wtf/)
