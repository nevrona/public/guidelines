# Guidelines

These guidelines are intended for everybody working with us. They are constantly evolving. **It is mandatory that you read them all** (it's not so long, it can take you up to 30' but it's probably going to take you less than 10').  
We intend to keep them small and compact, yet resourceful.

> IT professionals have a responsibility to understand the use of standards - *Tim Berners-Lee*

You can improve any guideline by properly branching and pushing your changes, then creating an MR.

## Confirm reading

After reading *all* of the guidelines:

1. Get the long commit hash of the *current version* of **this repo** (which can be obtained by running `git rev-parse origin/main`).
    * You can also copy it from the [gitlab interface](https://gitlab.com/nevrona/public/guidelines/-/commits/main).
    * We will call this value `current_version_commit_long_hash`.
1. Clone the [private signatories repo](https://gitlab.com/nevrona/devs/guidelines).
1. Get into the private signatories repo directory.
1. Create a branch named `signatory/{your name}` (where your name can actually be whatever you want to be identified as, such as a nickname).
1. Create a file with your name/alias/handle/id there with the following content: `I affirm that I have read and understood what is established in these guides for the version {current_version_commit_long_hash}.`
1. Then sign-off your commit (and write the commit message): `git commit --signoff` and push the branch: `git push origin signatory/{your name}`.

Finally, create a new merge request and assign a maintainer or your team leader for review.

## Update protocol

Every time a new version of the guidelines is created, we all need to sign them again. Therefore, we can not do this all the time and for that reason there is a protocol.

### Definitions

* Current version is the latest commit at the `main` branch.
* Next version is the latest commit at the `develop` branch.
* Release candidate is branched from `develop` and named `release`.
* Any proposed change lives in a `proposal/<proposal name>` branch and MAY be merged into `develop` (unless discarded).
* Merges SHOULD be "fast-forward" only.

### Protocol

1. Proposals SHOULD be revised and approved by fellows.
   * A merge request from a `proposal/...` branch is created to merge into `develop`.
   * Commits MUST be squashed into a single commit.
   * Rejected proposals MAY be removed from the server.
   * Rejected proposals MAY be presented again for review.
1. Approved proposals MUST be merged into `develop`.
   * Approved proposals MAY be removed in a future revision from any branch.
1. Every quarter of the year counting from the beginning of the year a new release is proposed by anyone through a merge request to `main` from a `release` branch.
   * The request is created if there are changes to merge.
   * A new `release` branch is created from `develop`.
   * Everybody MUST be notified of this event as soon as the request is created.
   * The maintainer MAY create this request if nobody else did.
1. After revision and approval of the people involved the request is merged into `main`, thus creating a new version of the guidelines.
   * The process is limited to up to a week.
   * If more than 50% of the total has approved, the request is merged.
   * If there is no consensus or not enough member's approvals, the request is closed without merging.
   * Changes MAY be made to `release` to reach consensus.
      * Changes made to `release` MUST be merged into `develop` if they are merged into `main`.
      * Changes made to `release` SHOULD be merged into `develop` if they are *not* merged into `main`.
   * The `release` branch MUST be removed after the request is merged or closed (unmerged changes are therefore discarded).
1. The new version MUST be signed by everybody.
   * The process is limited to up to a week, after which access to repositories is revoked util the guidelines are signed.

## License

Nevrona *Guidelines* is made by [Nevrona S. A.](https://nevrona.org) under `MPL-2.0`. You are free to use, share, modify and share modifications under the terms of that [license](LICENSE).

**Neither the name of Nevrona S. A. nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.**
